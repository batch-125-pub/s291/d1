let http = require("http");

const PORT = 3000;

http.createServer((request, response)=>{
	//URI (Uniform Resource Identifier)/Endpoint = resource -> request.url
	//http method -> request.method === "GET"
	if(request.url == "/profile" && request.method === "GET"){
		console.log(request);
		response.writeHead(200,
		{"Content-Type": "text/html"}
		);
		response.end(`Welcome to my page`);
	} else if(request.url == "/register" && request.method === "POST"){
		//console.log(`in post`);
		response.writeHead(200, 
			{"Content-Type": "text/html"});
		response.end(`Data to be sent to database`);
	} else {
		response.writeHead( 404,
			{"Content-Type": "text/plain"}
		);
		response.end(`This page is not found`);
	}

	
}).listen(PORT);
console.log(`Server is now connected to port: ${PORT}`);